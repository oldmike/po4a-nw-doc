## Contents

* [About this book](#About this book)
* [Scripts](#scripts)
* [Dependencies](#dependencies)
* [License](#license)

# About this book

This book describes how to translate noweb files (and tex files) using po4a.

The book is recently available in German. It is written in NoWeb and
contains a script.

The script create-book.sh generates the book in the format pdf and the script.

The generated book can be downloaded as [pdf][].

# Scripts

The script create-po4a.sh is available with a user interface in English.

The script create-book.sh has no user interface.

# Dependencies

To extract the program script and the book in pdf and epub format you
have to install the following packages:

For the scripts:

* noweb

and additional for creating the book

* texlive
* texlive-bibtex-extra
* texlive-binaries
* texlive-extra-utils
* texlive-lang-german
* texlive-lang-japanese
* texlive-latex-extra

To use the script you have to install the dependencies listed in the
header of the script.

# License

The book has the following license:

Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen
4.0 International Lizenz (CC BY-SA 4.0)
https://creativecommons.org/licenses/by-sa/4.0/legalcode

The code is licensed under GNU General Public License Version 3 or
(at your option) any later version.

