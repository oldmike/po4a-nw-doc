#!/bin/bash

# create-book.sh

# Copyright 2019-2023 Mechtilde and Michael Stehmann <mechtilde@debian.org>
# version 0.8.2

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

# Dependencies: 
# noweb po4a texlive-lang-german texlive-lang-japanese

#set -e

## Create PDF

BasePath=$(pwd)

noweave -index -delay po4a-nw-doc.nw > po4a-nw-doc.tex 

if [ -f po4a-nw-doc.aux ]
then
    rm po4a-nw-doc.aux
fi

for ((i=4; i>0; i--))
do
echo $i
pdflatex -shell-escape po4a-nw-doc.tex

# Create Index only one time
# Otherwise you get wrong page numbers
if [ i=4 ]
then
    makeindex po4a-nw-doc
fi

# Create Bibliography
# biber po4a-nw-doc
done

# tex4ebook -f epub po4a-nw-doc.tex ../

# For the first translation tests

#cd translation/en_US/target

# pdflatex -shell-escape ./po4a-nw-doc.tex

# cd ${BasePath}

# Remove auxillary *.html files which are needed for epub

# # if ls | grep --quiet '\.html'
# then
#    rm *.html
# fi

## Create script(s)

scripts="
  create-po4a.sh
"

t=`date +%c`

function build_script()
{
  notangle -R"$script" po4a-nw-doc.nw > "$script"
  sed --in-place \
    --expression='s/This is the end, my friend[[:cntrl:]]*/This is the end, my friend/' \
    "$1"
  echo "#generated on $t" >> "$1"
  chmod ugo+x "$1"
  bash -n "$1"
}

for script in $scripts; do
   build_script "$script"
done
