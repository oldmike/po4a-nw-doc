#!/bin/bash
# create-po4a.sh

# Copyright 2019-2021 Mechtilde and Michael Stehmann <mechtilde@debian.org>
# version 0.8.2

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

# Dependencies: 
# noweb po4a texlive-lang-german texlive-lang-japanese whiptail

# To work at the translation you may need additionally
# gtranslator texlive-lang-french translate-shell tidy

SRCDIR=$(pwd) # root of the documentation repository
POTDIR=${SRCDIR}/po4a/po # place where to create the pot
CONTENTDIR=${SRCDIR}/content


function CreateTex2Pot {
    # Called by CommonTasks
    # Create *.pot files from *.tex files

    cd ${SRCDIR}
    texL=$(ls GBP-*.tex)
    texA=($texL)
    for element in ${texA[*]}
    do
        # Delete file extention .tex
        element=$(echo ${element} | sed --expression='s/.tex//')
        po4a-gettextize --format latex --master-charset utf8 \
        --copyright-holder "Mechtilde & Michael Stehmann" \
        --package-name "BuildWithGBP" --package-version "Rolling Release" \
        --master ${element}.tex --po ${POTDIR}/${element}.pot
    done
}

function CreateNewLanguage {
    # Called by TasksSelect
    # Create a new language directory for translating

    newLang=$(whiptail --title "New language" \
    --inputbox "Please insert the ISO code of the new language:" \
    --cancel-button "Return" 15 60 3>&2 2>&1 1>&3)

    if [ -z "${newLang}" ]
    then
        return
    fi

    cd ${SRCDIR}
    if [ ! -d ${POTDIR}/${newLang} ]
    then
        # Copy all files which won't be translated like pictures etc.
        CONTENTDIRLANG=${CONTENTDIR}/${newLang}
        mkdir --parents ${CONTENTDIRLANG}
        mkdir --parents ${POTDIR}/${newLang}

        ln --symbolic ../../Pictures ${CONTENTDIRLANG}/Pictures
        cp BuildWithGBP.tex ${CONTENTDIRLANG}/
        cp Literatur.bib ${CONTENTDIRLANG}/
        cp .gitignore ${CONTENTDIRLANG}/
        cp .gitignore ${POTDIR}/${newLang}

        # Generating the *.po files for a new language
        cd ${POTDIR}
        newLangL=$(ls GBP-*.pot)
        newLangA=($newLangL)
        for elementLang in ${newLangA[*]}
        do
            elementLang=$(echo ${elementLang} | sed --expression='s/.pot//')
            msginit --input=${POTDIR}/${elementLang}.pot \
            --output-file=${POTDIR}/${newLang}/${elementLang}.po \
            --locale=${newLang} --no-translator
        done
    else
        whiptail --title "Important notice!" \
        --msgbox "${newLang} already exists!\nDid nothing!" 15 60
    fi
}

function MergeExistingLanguage {
    # Called TasksSelect
    # MergeExistingLanguage
    # Merging with existing *.po files

    texL=$(ls GBP-*.tex)
    texA=($texL)

    cd ${POTDIR}

    langL=$(ls --ignore="*.pot")
    langA=(${langL})

    for elementLang in ${langA[*]}
    do
        echo "Language: "${elementLang}

        for elementDoc in ${texA[*]}
        do
            elementDoc=$(echo ${elementDoc} | sed --expression='s/.tex//')
            if [ -f ${elementLang}/${elementDoc}.po ]
            then
                echo -e "File: "${elementDoc}"\n"
                msgmerge --verbose --update ${elementLang}/${elementDoc}.po  \
                ${elementDoc}.pot
                # po4a-updatepo --format latex --master ${elementDoc} \
                #--po ${elementDoc}.${elementLang}.po
            else
                echo -e ${elementLang}/${elementDoc}".po does not exist\n"
            fi
        done
    done
}

function CreateLocalizedBook {
    # Called by TasksSelect
    # Create a localized book (Po2Tex)
    # Generate a translated document
    # po4a po4a/po4a.cfg --verbose
    # pdflatex BuildWithGBP.CODE.tex

    cd ${POTDIR}
    langL=$(ls --ignore="*.pot")
    langA=(${langL})

    for elementLang in ${langA[*]}
    do
        echo "Language: "${elementLang}

        CONTENTDIRLANG=${CONTENTDIR}/${elementLang}
        mkdir --parents ${CONTENTDIRLANG}

        cd ${POTDIR}/${elementLang}
        POL=$(ls --ignore="*.po~")
        POA=($POL)
        for elementPO in ${POA[*]}
        do
            elementDoc=$(echo ${elementPO} | sed --expression='s/.po//')

            po4a-translate --format latex --master-charset utf8 \
            --master ${SRCDIR}/${elementDoc}.tex \
            --localized ${CONTENTDIRLANG}/${elementDoc}.tex \
            --po ${elementPO} --keep 30
            # the minimal threshold for translation percentage to keep
            # (i.e write) the resulting file is set to only 30 percent.
        done

        cd ${CONTENTDIRLANG}/
        makeindex BuildWithGBP.idx

        for ((i=4; i>0; i--))
        do
            pdflatex BuildWithGBP.tex
            # Literaturverzeichnis erstellen
            bibtex BuildWithGBP
        done
        # tex4ebook -f mobi BuildWithGBP.tex
        tex4ebook -f epub BuildWithGBP.tex ../

        # Remove auxillary *.html files which are needed for epub
        rm *.html
    done
}

function TranslationStatistics {
    # Called by TasksSelect
    # Translation Statistics with gettext
    
    # ToDo: über alle Dateien einer Sprache iterieren
    # msgfmt --statistics filename.po

    whiptail --msgbox "Work in progress" 15 60
}
# This is the end, my friend

#generated on So 15 Jan 2023 21:00:37 CET
